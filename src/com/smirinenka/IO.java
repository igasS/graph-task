package com.smirinenka;

import java.util.List;
import java.util.Scanner;

public class IO {

    public void readInput(List<Suburb> suburbs) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter input(enter 0 to end input):");
        String input = "";
        while (!input.equals("0")) {
            if (!(input = scanner.next()).equals("0")) {
                Suburb startPoint = new Suburb(input.charAt(0));
                Suburb endPoint = new Suburb(input.charAt(1));
                int distance = Integer.parseInt(input.substring(2));
                addSuburb(suburbs, startPoint, endPoint, distance);
            }
        }
    }

    private void addSuburb(List<Suburb> suburbs, Suburb startPoint, Suburb endPoint, int distance) {
        boolean listElementFound = false;
        for (Suburb suburb : suburbs) {
            if (suburb.getName() == startPoint.getName()) {
                suburb.getDirections().add(new Direction(endPoint.getName(), distance));
                listElementFound = true;
            }
        }
        if (!listElementFound) {
            startPoint.getDirections().add(new Direction(endPoint.getName(), distance));
            suburbs.add(startPoint);
        }
    }

    public void printResult(int result) {
        if (result == 0) {
            System.out.println("NO SUCH ROUTE");
        } else {
            System.out.println(result);
        }
    }

}