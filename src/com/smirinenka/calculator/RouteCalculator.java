package com.smirinenka.calculator;

import com.smirinenka.Direction;
import com.smirinenka.Suburb;
import java.util.ArrayList;
import java.util.List;

public class RouteCalculator {

    private int distance;
    private int amountOfRoutes;
    private String firstSuburbName;
    private String route;
    private List<String> routes = new ArrayList<String>();

    public int calculateAmountOfRoutesWithDistance(List<Suburb> suburbs, char startSuburbName, char endSuburbName, int maxDistance) {
        distance = 0;
        amountOfRoutes = 0;
        route = "" + startSuburbName;
        firstSuburbName = "" + startSuburbName;
        routes.clear();
        findRoutesWithDistance(suburbs, findSuburb(suburbs, startSuburbName), endSuburbName, maxDistance);
        return amountOfRoutes;
    }

    private void findRoutesWithDistance(List<Suburb> suburbs, Suburb startSuburb, char endSuburbName, int maxDistance) {
        for (Direction direction : startSuburb.getDirections()) {
            route += direction.getSuburbName();
            distance += direction.getDistance();
            if (distance < maxDistance) {
                if(!routes.contains(route)) {
                    if (direction.getSuburbName() == endSuburbName) {
                        amountOfRoutes++;
                        distance = 0;
                        routes.add(route);
                        route = firstSuburbName;
                    } else {
                        findRoutesWithDistance(suburbs, findSuburb(suburbs, direction.getSuburbName()), endSuburbName, maxDistance);
                    }
                } else {
                    findRoutesWithDistance(suburbs, findSuburb(suburbs, direction.getSuburbName()), endSuburbName, maxDistance);
                }
            } else {
                return;
            }
        }
    }

    private Suburb findSuburb(List<Suburb> suburbs, char name) {
        Suburb result = null;
        for (Suburb suburb : suburbs) {
            if (suburb.getName() == name) {
                result = suburb;
            }
        }
        return result;
    }
}