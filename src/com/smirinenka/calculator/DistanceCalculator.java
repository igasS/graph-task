package com.smirinenka.calculator;

import com.smirinenka.Direction;
import com.smirinenka.Suburb;

import java.util.List;

public class DistanceCalculator {

    private int distance;
    private boolean routeNotFound;

    public int calculateRouteDistance(List<Suburb> suburbs, String route) {
        distance = 0;
        routeNotFound = false;
        for (int i = 0; i < route.toCharArray().length; i++) {
            Suburb suburb = findSuburb(suburbs, route.toCharArray()[i]);
            if (suburb != null) {
                if ((i + 1) < route.toCharArray().length) {
                    routeNotFound = true;
                    for (Direction direction : suburb.getDirections()) {
                        if (direction.getSuburbName() == route.toCharArray()[i + 1]) {
                            distance += direction.getDistance();
                            routeNotFound = false;
                        }
                    }
                }
            }
        }
        if (routeNotFound) {
            return 0;
        } else {
            return distance;
        }
    }

    private Suburb findSuburb(List<Suburb> suburbs, char name) {
        Suburb result = null;
        for (Suburb suburb : suburbs) {
            if (suburb.getName() == name) {
                result = suburb;
            }
        }
        return result;

    }

}