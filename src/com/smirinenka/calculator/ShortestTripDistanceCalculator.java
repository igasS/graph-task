package com.smirinenka.calculator;

import com.smirinenka.Direction;
import com.smirinenka.Suburb;

import java.util.List;

public class ShortestTripDistanceCalculator {

    private int minDistance;
    private int distance;
    private int firstStepDistance;
    private String visitedDirections;

    public int calculateShortestRouteDistance(List<Suburb> suburbs, char startSuburbName, char endSuburbName) {
        Suburb startSuburb = findSuburb(suburbs, startSuburbName);
        minDistance = 100;
        distance = 0;
        firstStepDistance = startSuburb.getDirections().get(0).getDistance();
        visitedDirections = "";
        findRoutesDistances(suburbs, startSuburb, endSuburbName);
        if (minDistance != 100) {
            return minDistance;
        } else {
            return 0;
        }
    }

    private void findRoutesDistances(List<Suburb> suburbs, Suburb startSuburb, char endSuburbName) {
        for (Direction direction : startSuburb.getDirections()) {
            if (!hasDirection(visitedDirections, direction.getSuburbName())) {
                distance += direction.getDistance();
                visitedDirections += direction.getSuburbName();
                if (direction.getSuburbName() == endSuburbName) {
                    minDistance = (distance < minDistance) ? distance : minDistance;
                    distance = firstStepDistance;
                    visitedDirections = "";
                    return;
                } else {
                    findRoutesDistances(suburbs, findSuburb(suburbs, direction.getSuburbName()), endSuburbName);
                }
            }
        }
    }

    private boolean hasDirection(String directions, char directionName) {
        boolean result = false;
        for (char item : directions.toCharArray()) {
            if (item == directionName) {
                result = true;
            }
        }
        return result;
    }

    private Suburb findSuburb(List<Suburb> suburbs, char name) {
        Suburb result = null;
        for (Suburb suburb : suburbs) {
            if (suburb.getName() == name) {
                result = suburb;
            }
        }
        return result;
    }

}