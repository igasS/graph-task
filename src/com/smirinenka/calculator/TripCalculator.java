package com.smirinenka.calculator;

import com.smirinenka.Direction;
import com.smirinenka.Suburb;

import java.util.List;

public class TripCalculator {

    private int amountOfRoutes;
    private int stops;

    public int calculateAmountOfRoutes(List<Suburb> suburbs, char startSuburbName, char endSuburbName, int maxStops) {
        amountOfRoutes = 0;
        stops = 0;
        lookForRoute(suburbs, findSuburb(suburbs, startSuburbName), endSuburbName, maxStops);
        return amountOfRoutes;
    }

    private void lookForRoute(List<Suburb> suburbs, Suburb startSuburb, char endSuburbName, int maxStops) {
        stops++;
        for (Direction direction : startSuburb.getDirections()) {
            if (stops <= maxStops) {
                if (direction.getSuburbName() == endSuburbName) {
                    amountOfRoutes++;
                    stops = 0;
                    return;
                } else {
                    lookForRoute(suburbs, findSuburb(suburbs, direction.getSuburbName()), endSuburbName, maxStops);
                }
            } else {
                return;
            }
        }
    }

    public int calculateAmountOfRoutesWithStops(List<Suburb> suburbs, char startSuburbName, char endSuburbName, int stopsAmount) {
        amountOfRoutes = 0;
        stops = 0;
        lookForRouteWithStops(suburbs, findSuburb(suburbs, startSuburbName), endSuburbName, stopsAmount);
        return amountOfRoutes;
    }

    private void lookForRouteWithStops(List<Suburb> suburbs, Suburb startSuburb, char endSuburbName, int stopsAmount) {
        stops++;
        for (Direction direction : startSuburb.getDirections()) {
            if (direction.getSuburbName() == endSuburbName) {
                if (stops == stopsAmount) {
                    amountOfRoutes++;
                    stops = 0;
                    return;
                }
            } else {
                lookForRoute(suburbs, findSuburb(suburbs, direction.getSuburbName()), endSuburbName, stopsAmount);
            }
        }
    }

    private Suburb findSuburb(List<Suburb> suburbs, char name) {
        Suburb result = null;
        for (Suburb suburb : suburbs) {
            if (suburb.getName() == name) {
                result = suburb;
            }
        }
        return result;
    }

}
