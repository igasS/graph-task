package com.smirinenka;

import java.util.ArrayList;
import java.util.List;

public class Suburb {

    private char name;
    private List<Direction> directions = new ArrayList<Direction>();

    public Suburb(char name) {
        this.name = name;
    }

    public char getName() {
        return name;
    }

    public List<Direction> getDirections() {
        return directions;
    }

}