package com.smirinenka;

import com.smirinenka.calculator.RouteCalculator;
import com.smirinenka.calculator.DistanceCalculator;
import com.smirinenka.calculator.ShortestTripDistanceCalculator;
import com.smirinenka.calculator.TripCalculator;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        IO io = new IO();
        List<Suburb> suburbs = new ArrayList<Suburb>();
        io.readInput(suburbs);

        DistanceCalculator distanceCalculator = new DistanceCalculator();
        io.printResult(distanceCalculator.calculateRouteDistance(suburbs, "ABC"));
        io.printResult(distanceCalculator.calculateRouteDistance(suburbs, "AD"));
        io.printResult(distanceCalculator.calculateRouteDistance(suburbs, "ADC"));
        io.printResult(distanceCalculator.calculateRouteDistance(suburbs, "AEBCD"));
        io.printResult(distanceCalculator.calculateRouteDistance(suburbs, "AED"));

        TripCalculator tripCalculator = new TripCalculator();
        io.printResult(tripCalculator.calculateAmountOfRoutes(suburbs, 'C', 'C', 3));
        io.printResult(tripCalculator.calculateAmountOfRoutesWithStops(suburbs, 'A', 'C', 4));

        ShortestTripDistanceCalculator shortestTripDistanceCalculator = new ShortestTripDistanceCalculator();
        io.printResult(shortestTripDistanceCalculator.calculateShortestRouteDistance(suburbs, 'A', 'C'));
        io.printResult(shortestTripDistanceCalculator.calculateShortestRouteDistance(suburbs, 'B', 'B'));

        RouteCalculator routeCalculator = new RouteCalculator();
        io.printResult(routeCalculator.calculateAmountOfRoutesWithDistance(suburbs, 'C', 'C', 30));
    }

}
