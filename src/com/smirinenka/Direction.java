package com.smirinenka;

public class Direction {

    private char suburbName;
    private int distance;

    public Direction(char suburbName, int distance) {
        this.suburbName = suburbName;
        this.distance = distance;
    }

    public char getSuburbName() {
        return suburbName;
    }

    public int getDistance() {
        return distance;
    }

}