import com.smirinenka.calculator.RouteCalculator;
import com.smirinenka.Direction;
import com.smirinenka.Suburb;
import com.smirinenka.calculator.DistanceCalculator;
import com.smirinenka.calculator.ShortestTripDistanceCalculator;
import com.smirinenka.calculator.TripCalculator;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    @Test
    public void should_test_if_method_counts_route_distance() throws Exception {
        List<Suburb> suburbs = new ArrayList<Suburb>();
        List<Direction> directions = new ArrayList<Direction>();

        directions.add(new Direction('B', 5));
        directions.add(new Direction('D', 5));
        directions.add(new Direction('E', 7));
        Suburb suburb = new Suburb('A');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('C', 4));
        suburb = new Suburb('B');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('D', 8));
        directions.add(new Direction('E', 2));
        suburb = new Suburb('C');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('C', 8));
        directions.add(new Direction('E', 6));
        suburb = new Suburb('D');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('B', 3));
        suburb = new Suburb('E');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        DistanceCalculator distanceCalculator = new DistanceCalculator();
        assertEquals(9, distanceCalculator.calculateRouteDistance(suburbs, "ABC"));
    }

    @Test
    public void should_test_if_method_counts_amount_of_routes_with_less_than_given_amount_of_stops() throws Exception {
        List<Suburb> suburbs = new ArrayList<Suburb>();
        List<Direction> directions = new ArrayList<Direction>();

        directions.add(new Direction('B', 5));
        directions.add(new Direction('D', 5));
        directions.add(new Direction('E', 7));
        Suburb suburb = new Suburb('A');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('C', 4));
        suburb = new Suburb('B');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('D', 8));
        directions.add(new Direction('E', 2));
        suburb = new Suburb('C');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('C', 8));
        directions.add(new Direction('E', 6));
        suburb = new Suburb('D');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('B', 3));
        suburb = new Suburb('E');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        TripCalculator tripCalculator = new TripCalculator();
        assertEquals(2, tripCalculator.calculateAmountOfRoutes(suburbs, 'C', 'C', 3));
    }

    @Test
    public void should_test_if_method_counts_number_of_trips_with_set_amount_of_stops() throws Exception {
        List<Suburb> suburbs = new ArrayList<Suburb>();
        List<Direction> directions = new ArrayList<Direction>();

        directions.add(new Direction('B', 5));
        directions.add(new Direction('D', 5));
        directions.add(new Direction('E', 7));
        Suburb suburb = new Suburb('A');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('C', 4));
        suburb = new Suburb('B');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('D', 8));
        directions.add(new Direction('E', 2));
        suburb = new Suburb('C');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('C', 8));
        directions.add(new Direction('E', 6));
        suburb = new Suburb('D');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('B', 3));
        suburb = new Suburb('E');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        TripCalculator tripCalculator = new TripCalculator();
        assertEquals(3, tripCalculator.calculateAmountOfRoutesWithStops(suburbs, 'A', 'C', 4));
    }

    @Test
    public void should_test_if_method_counts_shortest_route_distance() throws Exception {
        List<Suburb> suburbs = new ArrayList<Suburb>();
        List<Direction> directions = new ArrayList<Direction>();

        directions.add(new Direction('B', 5));
        directions.add(new Direction('D', 5));
        directions.add(new Direction('E', 7));
        Suburb suburb = new Suburb('A');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('C', 4));
        suburb = new Suburb('B');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('D', 8));
        directions.add(new Direction('E', 2));
        suburb = new Suburb('C');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('C', 8));
        directions.add(new Direction('E', 6));
        suburb = new Suburb('D');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('B', 3));
        suburb = new Suburb('E');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        ShortestTripDistanceCalculator shortestTripDistanceCalculator = new ShortestTripDistanceCalculator();
        assertEquals(9, shortestTripDistanceCalculator.calculateShortestRouteDistance(suburbs, 'B', 'B'));
    }

    @Test
    public void should_test_if_method_counts_amount_of_routes_with_less_than_given_distance() throws Exception {
        List<Suburb> suburbs = new ArrayList<Suburb>();
        List<Direction> directions = new ArrayList<Direction>();

        directions.add(new Direction('B', 5));
        directions.add(new Direction('D', 5));
        directions.add(new Direction('E', 7));
        Suburb suburb = new Suburb('A');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('C', 4));
        suburb = new Suburb('B');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('D', 8));
        directions.add(new Direction('E', 2));
        suburb = new Suburb('C');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('C', 8));
        directions.add(new Direction('E', 6));
        suburb = new Suburb('D');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        directions.add(new Direction('B', 3));
        suburb = new Suburb('E');
        suburb.getDirections().addAll(directions);
        suburbs.add(suburb);
        directions.clear();

        RouteCalculator routeCalculator = new RouteCalculator();
        assertEquals(7, routeCalculator.calculateAmountOfRoutesWithDistance(suburbs, 'C', 'C', 30));
    }
}